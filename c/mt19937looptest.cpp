#include <iostream>
#include <random>

int main()
{
	std::random_device seed_gen;
	std::mt19937 engine(seed_gen());
	std::uint32_t result;
	for (int i = 0; i < 10000; i++)
	{
		result = engine();
		std::cout << result << std::endl;
	}
}